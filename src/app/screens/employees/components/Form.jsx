import React, { Component } from 'react';
import { Breadcrumb } from '@gull';
import { useSelector } from 'react-redux';
import { name } from '..';
import { Formik } from 'formik';
import * as yup from 'yup';
import DateTime from 'react-datetime';
import { Tabs, Tab } from 'react-bootstrap';

import * as lang from './lang';
import { GLOBAL_GENDER, GLOBAL_LOCATION } from '../../../constants/options';
import * as ROUTERS from '../../../constants/router';

export default function FormData() {
  const { data } = useSelector((state) => state[name]);
  const handleSubmit = (value) => {
    console.log(value);
  };
  return (
    <div>
      <Breadcrumb
        routeSegments={[
          { name: lang.EMPLOYEE_VN.home, path: ROUTERS.DASHBOARD.DEFAULT },
          { name: lang.EMPLOYEE_VN.page, path: ROUTERS.EMPLOYEES.DEFAULT },
          {
            name: !!data && !!data.employeeNumber ? data.employeeNumber.toUpperCase() : lang.EMPLOYEE_VN.headerForm
          }
        ]}
      />

      <section className="basic-action-bar card">
        <div className="card-body">
          <Formik initialValues={data} validationSchema={FormSchema} onSubmit={handleSubmit}>
            {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <form onSubmit={handleSubmit}>
                <div className="row" style={{ overflow: 'auto', height: '65vh' }}>
                  <div className="col-md-12 card-header bg-transparent">
                    <h3 className="card-title">{lang.EMPLOYEE_VN.primaryInfo}</h3>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="text">{lang.EMPLOYEE_VN.firstName} (*)</label>
                      <input
                        className="form-control"
                        name="firstName"
                        type="text"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      {errors.firstName && touched.firstName && (
                        <div className="text-danger mt-1 ml-2">{errors.firstName}</div>
                      )}
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="text">{lang.EMPLOYEE_VN.lastName} (*)</label>
                      <input
                        className="form-control"
                        name="lastName"
                        type="text"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      {errors.lastName && touched.lastName && (
                        <div className="text-danger mt-1 ml-2">{errors.lastName}</div>
                      )}
                    </div>
                  </div>
                  <div className="col-md-6 form-group mb-3">
                    <label htmlFor="picker1">{lang.EMPLOYEE_VN.gender}</label>
                    <select
                      id="gender"
                      className="form-control"
                      name="gender"
                      onChange={handleChange}
                      onBlur={handleBlur}>
                      {GLOBAL_GENDER.map((o) => (
                        <option value={o}>{lang.EMPLOYEE_VN[o]}</option>
                      ))}
                    </select>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="text">{lang.EMPLOYEE_VN.dob}</label>
                      <DateTime timeFormat={false} />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="text">{lang.EMPLOYEE_VN.phone}</label>
                      <input
                        className="form-control"
                        name="phone"
                        type="text"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="email">{lang.EMPLOYEE_VN.email}</label>
                      <input
                        className="form-control"
                        name="email"
                        type="text"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="text">{lang.EMPLOYEE_VN.address}</label>
                      <input
                        className="form-control"
                        name="address"
                        type="text"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="text">{lang.EMPLOYEE_VN.notePerson}</label>
                      <input
                        className="form-control"
                        name="notePerson"
                        type="text"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                  </div>
                  <div className="col-md-12 card-header bg-transparent">
                    <h3 className="card-title">{lang.EMPLOYEE_VN.otherInfo}</h3>
                  </div>
                  <Tabs defaultActiveKey="department" className="justify-content-start profile-nav mb-4">
                    <Tab eventKey="department" title={lang.EMPLOYEE_VN.department}>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text">{lang.EMPLOYEE_VN.suppervisor}</label>
                            <input
                              className="form-control"
                              name="suppervisor"
                              type="text"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>{' '}
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text">{lang.EMPLOYEE_VN.department}</label>
                            <input
                              className="form-control"
                              name="department"
                              type="text"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>{' '}
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text">{lang.EMPLOYEE_VN.position}</label>
                            <input
                              className="form-control"
                              name="position"
                              type="text"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>{' '}
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text">{lang.EMPLOYEE_VN.noteDepartment}</label>
                            <input
                              className="form-control"
                              name="noteDepartment"
                              type="text"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>{' '}
                      </div>
                    </Tab>
                    <Tab eventKey="account" title={lang.EMPLOYEE_VN.accountInfo}>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text">{lang.EMPLOYEE_VN.typePayment}</label>
                            <input
                              className="form-control"
                              name="typePayment"
                              type="text"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>{' '}
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text">{lang.EMPLOYEE_VN.numberPayment}</label>
                            <input
                              className="form-control"
                              name="numberPayment"
                              type="text"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>{' '}
                        <div className="col-md-12">
                          <div className="form-group">
                            <label htmlFor="text">{lang.EMPLOYEE_VN.noteDepartment}</label>
                            <input
                              className="form-control"
                              name="noteDepartment"
                              type="text"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>{' '}
                      </div>
                    </Tab>
                    <Tab eventKey="quanlificationInfo" title={lang.EMPLOYEE_VN.quanlificationInfo}>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text">{lang.EMPLOYEE_VN.nameQuanlifi}</label>
                            <input
                              className="form-control"
                              name="nameQuanlifi"
                              type="text"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>{' '}
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text">{lang.EMPLOYEE_VN.noteQuanlifi}</label>
                            <input
                              className="form-control"
                              name="noteQuanlifi"
                              type="text"
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>{' '}
                      </div>
                    </Tab>
                  </Tabs>
                </div>
                <div className="card-footer bg-transparent">
                  <div className="mc-footer">
                    <div className="row">
                      <div className="col-lg-12">
                        <button type="button" className="btn  btn-primary m-1">
                          {lang.EMPLOYEE_VN.save}
                        </button>
                        <button type="button" className="btn btn-outline-secondary m-1">
                          {lang.EMPLOYEE_VN.cancel}
                        </button>
                        <button type="button" className="btn  btn-danger m-1 footer-delete-right">
                          {lang.EMPLOYEE_VN.delete}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </section>
    </div>
  );
}

const FormSchema = yup.object().shape({
  firstName: yup.string().required(`${lang.EMPLOYEE_VN.firstName} ${lang.EMPLOYEE_VN.cautionMandantory}`),
  lastName: yup.string().required(`${lang.EMPLOYEE_VN.lastName} ${lang.EMPLOYEE_VN.cautionMandantory}`),
  email: yup.string().email(lang.EMPLOYEE_VN.cautionEmailFomat)
});
