export const GLOBAL_TABLE_VN = {
  firstPageText: 'Trang đầu',
  prePageText: 'Trước',
  nextPageText: 'Sau',
  lastPageText: 'Trang cuối cùng',
  nextPageTitle: 'Tiếp theo',
  prePageTitle: 'Trước đó',
  firstPageTitle: 'Trang đầu',
  lastPageTitle: 'Trang cuối cùng',
  searchText: 'Tìm kiếm',
  tableEmpty: 'Danh Sách trống'
};
