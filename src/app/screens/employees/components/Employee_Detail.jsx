import React, { useState } from 'react';
import { Breadcrumb } from '@gull';
import { Tabs, Tab } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { EMPLOYEE_VN } from './lang';
import * as ROUTERS from '../../../constants/router';
import { useSelector } from 'react-redux';
import moment from 'moment';
import { name } from '..';
import * as lang from './lang';
export default function EmployeeDetail() {
  const { data } = useSelector((state) => state[name]);
  const [tab, setTab] = useState('about');
  return (
    <>
      <Breadcrumb
        routeSegments={[
          { name: EMPLOYEE_VN.home, path: ROUTERS.DASHBOARD.DEFAULT },
          { name: EMPLOYEE_VN.page, path: ROUTERS.EMPLOYEES.DEFAULT },
          { name: data.employeeNumber.toUpperCase() }
        ]}
      />

      <div className="card user-profile o-hidden mb-4">
        <div
          className="header-cover"
          style={{
            backgroundImage: "url('/assets/images/photo-wide-5.jpeg')"
          }}></div>
        <div className="user-info">
          <img className="profile-picture avatar-lg mb-2" src="/assets/images/faces/1.jpg" alt="" />
          <p className="m-0 text-24">
            {data.firstName} {data.lastName}
          </p>
          <p className="text-muted m-0">{data.position}</p>
        </div>
        <div className="card-body pt-4">
          <Tabs defaultActiveKey={tab} className="justify-content-center profile-nav mb-4">
            <Tab eventKey="about" title={lang.EMPLOYEE_VN.about}>
              <h4>{lang.EMPLOYEE_VN.primaryInfo}</h4>
              <p>
                {!!data && !!data.notePerson
                  ? data.notePerson
                  : `Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, commodi quam! Provident quis voluptate
                asperiores ullam, quidem odio pariatur. Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Voluptatem, nulla eos? Cum non ex voluptate corporis id asperiores doloribus dignissimos blanditiis
                iusto qui repellendus deleniti aliquam, vel quae eligendi explicabo.`}
              </p>
              <hr />
              <div className="row">
                <div className="col-md-4 col-6">
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Calendar text-16 mr-1"></i> {lang.EMPLOYEE_VN.dob}
                    </p>
                    <span>{moment(data.dob).format('LL')}</span>
                  </div>
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Edit-Map text-16 mr-1"></i> {lang.EMPLOYEE_VN.address}
                    </p>
                    <span>{data.address}</span>
                  </div>
                </div>
                <div className="col-md-4 col-6">
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-MaleFemale text-16 mr-1"></i> {lang.EMPLOYEE_VN.gender}
                    </p>
                    <span>{lang.EMPLOYEE_VN[data.gender]}</span>
                  </div>
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Business-Man text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.position}
                    </p>
                    <span>{data.position}</span>
                  </div>
                </div>
                <div className="col-md-4 col-6">
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-MaleFemale text-16 mr-1"></i> {lang.EMPLOYEE_VN.email}
                    </p>
                    <span>{data.email}</span>
                  </div>
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Conference text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.phone}
                    </p>
                    <span>{data.phone}</span>
                  </div>
                </div>
              </div>
              <hr />
              <h4>{lang.EMPLOYEE_VN.otherInfo}</h4>
              <p className="mb-4">
                {!!data && !!data.noteDepartment
                  ? data.noteDepartment
                  : `Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, commodi quam! Provident quis voluptate
                asperiores ullam, quidem odio pariatur. Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Voluptatem, nulla eos? Cum non ex voluptate corporis id asperiores doloribus dignissimos blanditiis
                iusto qui repellendus deleniti aliquam, vel quae eligendi explicabo.`}
              </p>
              <div className="row">
                <div className="col-md-4 col-6">
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Business-Man text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.suppervisor}
                    </p>
                    <span>{data.suppervisor}</span>
                  </div>
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Business-Man text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.department}
                    </p>
                    <span>{data.department}</span>
                  </div>
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Conference text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.level}
                    </p>
                    <span>{data.level}</span>
                  </div>
                </div>
                <div className="col-md-4 col-6">
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Conference text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.salary}
                    </p>
                    <span>{data.salary}</span>
                  </div>{' '}
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Conference text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.typePayment}
                    </p>
                    <span>{data.typePayment}</span>
                  </div>
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Conference text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.numberPayment}
                    </p>
                    <span>{data.numberPayment}</span>
                  </div>
                </div>
                <div className="col-md-4 col-6">
                  {' '}
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Conference text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.nameQuanlifi}
                    </p>
                    <span>{data.nameQuanlifi}</span>
                  </div>{' '}
                  <div className="mb-4">
                    <p className="text-primary mb-1">
                      <i className="i-Conference text-16 mr-1"></i>
                      {lang.EMPLOYEE_VN.noteQuanlifi}
                    </p>
                    <span>{data.noteQuanlifi}</span>
                  </div>
                </div>
              </div>
            </Tab>
            <Tab eventKey="timeline" title={lang.EMPLOYEE_VN.timeline}>
              <div className="tab-pane fade active show" id="timeline" role="tabpanel" aria-labelledby="timeline-tab">
                <ul className="timeline clearfix">
                  <li className="timeline-line"></li>
                  <li className="timeline-item">
                    <div className="timeline-badge">
                      <i className="badge-icon bg-primary text-white i-Cloud-Laptop"></i>
                    </div>
                    <div className="timeline-card card">
                      <div className="card-body">
                        <div className="mb-1">
                          <strong className="mr-1">Timothy Carlson</strong>
                          added a new photo
                          <p className="text-muted">3 hours ago</p>
                        </div>
                        <img className="rounded mb-2" src="/assets/images/photo-wide-1.jpg" alt="" />
                        <div className="mb-3">
                          <span className="mr-1">Like</span>
                          <span>Comment</span>
                        </div>
                        <div className="input-group">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Write comment"
                            aria-label="comment"
                          />
                          <div className="input-group-append">
                            <button className="btn btn-primary" type="button" id="button-comment1">
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li className="timeline-item">
                    <div className="timeline-badge">
                      <img className="badge-img" src="/assets/images/faces/1.jpg" alt="" />
                    </div>
                    <div className="timeline-card card">
                      <div className="card-body">
                        <div className="mb-1">
                          <strong className="mr-1">Timothy Carlson</strong>
                          updated his sattus
                          <p className="text-muted">16 hours ago</p>
                        </div>
                        <p>
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi dicta beatae illo illum iusto
                          iste mollitia explicabo quam officia. Quas ullam, quisquam architecto aspernatur enim iure
                          debitis dignissimos suscipit ipsa.
                        </p>
                        <div className="mb-3">
                          <span className="mr-1">Like</span>
                          <span>Comment</span>
                        </div>
                        <div className="input-group">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Write comment"
                            aria-label="comment"
                          />
                          <div className="input-group-append">
                            <button className="btn btn-primary" type="button" id="button-comment2">
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
                <ul className="timeline clearfix">
                  <li className="timeline-line"></li>
                  <li className="timeline-group text-center">
                    <button className="btn btn-icon-text btn-primary">
                      <i className="i-Calendar-4"></i> 2018
                    </button>
                  </li>
                </ul>
                <ul className="timeline clearfix">
                  <li className="timeline-line"></li>
                  <li className="timeline-item">
                    <div className="timeline-badge">
                      <i className="badge-icon bg-danger text-white i-Love-User"></i>
                    </div>
                    <div className="timeline-card card">
                      <div className="card-body">
                        <div className="mb-1">
                          <strong className="mr-1">New followers</strong>
                          <p className="text-muted">2 days ago</p>
                        </div>
                        <p>
                          <Link to="/">Henry krick</Link> and 16 others followed you
                        </p>
                        <div className="mb-3">
                          <span className="mr-1">Like</span>
                          <span>Comment</span>
                        </div>
                        <div className="input-group">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Write comment"
                            aria-label="comment"
                          />
                          <div className="input-group-append">
                            <button className="btn btn-primary" type="button" id="button-comment3">
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li className="timeline-item">
                    <div className="timeline-badge">
                      <i className="badge-icon bg-primary text-white i-Cloud-Laptop"></i>
                    </div>
                    <div className="timeline-card card">
                      <div className="card-body">
                        <div className="mb-1">
                          <strong className="mr-1">Timothy Carlson</strong>
                          added a new photo
                          <p className="text-muted">2 days ago</p>
                        </div>
                        <img className="rounded mb-2" src="/assets/images/photo-wide-2.jpg" alt="" />
                        <div className="mb-3">
                          <span className="mr-1">Like</span>
                          <span>Comment</span>
                        </div>
                        <div className="input-group">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Write comment"
                            aria-label="comment"
                          />
                          <div className="input-group-append">
                            <button className="btn btn-primary" type="button" id="button-comment4">
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
                <ul className="timeline clearfix">
                  <li className="timeline-line"></li>
                  <li className="timeline-group text-center">
                    <button className="btn btn-icon-text btn-warning">
                      <i className="i-Calendar-4"></i> Joined in 2013
                    </button>
                  </li>
                </ul>
              </div>
            </Tab>
          </Tabs>
        </div>
      </div>
    </>
  );
}
