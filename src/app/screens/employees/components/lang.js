export const EMPLOYEE_VN = {
  home: 'Trang chủ',
  page: 'Danh sách nhân viên',
  employeeNumber: 'Mã nhân viên',
  firstName: 'Họ',
  lastName: 'Tên',
  dob: 'Ngày sinh',
  gender: 'Giới Tính',
  phone: 'Điện Thoại',
  email: 'Địa chỉ E-mail',
  address: 'Địa chỉ',
  village: 'Xã',
  district: 'Huyện',
  city: 'Thành Phố',
  province: 'Tỉnh',
  certificate: 'Số CMND/CCCD',
  notePerson: 'Ghi chú',
  suppervisor: 'Người giám sát',
  department: 'Tên Phòng bang',
  position: 'Chức vụ',
  salary: 'Mức lương hiện tại',
  typePayment: 'Loại hình thanh toán',
  numberPayment: 'Mã số thẻ/ mã tài khoản thanh toán',
  noteDepartment: 'Ghi chú',
  nameQuanlifi: 'Tên',
  level: 'Mức độ đánh giá (1 - 5)',
  noteQuanlifi: 'Ghi chú',
  name: 'Họ và tên',
  createdAt: 'Thời gian bắt đầu',
  //groups
  primaryInfo: 'Thông tin chính',
  departmentInfo: 'Thông tin phòng ban',
  accountInfo: 'Thông tin kế toán',
  quanlificationInfo: 'Trình độ học vấn',
  otherInfo: 'Thông tin khác',
  // employee detail
  about: 'Thông tin cá nhân',
  timeline: 'Dòng thời gian',
  male: 'Nam',
  female: 'Nữ',
  other: 'Khác',
  //form
  headerForm: 'Thêm mới nhân viên',
  // buttons
  save: 'Lưu lại',
  cancel: 'Hủy',
  delete: 'Xóa',
  view: 'Xem',
  update: 'Cập nhật',
  search: 'Tìm kiếm',
  searchPlaceHold: 'Tìm kiếm theo họ và tên, số điện thoại',
  add: 'Thêm mới',
  export: 'Xuất dữ liệu'
};

export const MOCK_DATA = [
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  },
  {
    employeeNumber: 'NV',
    firstName: 'Thọ',
    lastName: 'Nguyễn Thanh',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    gender: 'man',
    position: 'Nhân viên',
    createdAt: new Date()
  }
];
