import React, { useEffect, useState } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search, CSVExport } from 'react-bootstrap-table2-toolkit';
import { SimpleCard } from '@gull';
import { Row, Col, Button, InputGroup, FormControl, SplitButton, Dropdown } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { Breadcrumb } from '@gull';

import { useQuery } from '../../../services/routerServices';
import * as ROUTERS from '../../../constants/router';
import { GLOBAL_TABLE_VN } from '../../../constants/table.lang';
import * as lang from './lang';
import { columns, paginationOptions, mapDataTable } from './utilities';
export default function HandleTable() {
  const history = useHistory();
  const query = useQuery();
  const { pathname } = history.location;

  const [pagination, setPagination] = useState({
    ...GLOBAL_TABLE_VN,
    paginationSize: parseInt(query.get('size') || 10),
    pageStartIndex: parseInt(query.get('page') || 1),
    search: query.get('search') || '',
    showTotal: true,
    totalSize: lang.MOCK_DATA.length,
    btnGrp: query.get('btnGrp') || 'add'
  });

  useEffect(() => {
    history.push({
      pathname,
      search: `size=${pagination.paginationSize}&page=${pagination.pageStartIndex}&search=${pagination.search}&total=${pagination.totalSize}&btnGrp=${pagination.btnGrp}`
    });
  }, []);

  function handleSearch(e) {
    if (e.key === 'Enter') {
      history.push({
        pathname,
        search: `size=${pagination.paginationSize}&page=${pagination.pageStartIndex}&search=${e.target.value}&total=${pagination.totalSize}&btnGrp=${pagination.btnGrp}`
      });
      setPagination({ ...pagination, search: e.target.value });
    }
  }
  function handleBtnGrp(e) {
    history.push(
      `${pathname}?size=${pagination.paginationSize}&page=${pagination.pageStartIndex}&search=${pagination.search}&total=${pagination.totalSize}&btnGrp=${e}`
    );
    setPagination({ ...pagination, btnGrp: e });
  }
  console.log(pagination);
  return (
    <>
      <Breadcrumb
        routeSegments={[
          { name: lang.EMPLOYEE_VN.home, path: ROUTERS.DASHBOARD.DEFAULT },
          { name: lang.EMPLOYEE_VN.page, path: ROUTERS.EMPLOYEES.DEFAULT }
        ]}
      />
      <Row>
        <Col lg={12} md={12} sm={12} xs={12} className="mb-4">
          <SimpleCard>
            <ToolkitProvider striped keyField="id" data={mapDataTable(lang.MOCK_DATA)} columns={columns} search>
              {(props) => (
                <>
                  <div className="d-flex justify-content-end">
                    <InputGroup className="mb-3" onKeyPress={handleSearch}>
                      <FormControl defaultValue={pagination.search} />
                      <SplitButton
                        variant="outline-primary"
                        title={lang.EMPLOYEE_VN[pagination.btnGrp]}
                        id="segmented-button-dropdown-2"
                        alignRight>
                        <Dropdown.Item onClick={() => handleBtnGrp('add')}>{lang.EMPLOYEE_VN.add}</Dropdown.Item>
                        <Dropdown.Item onClick={() => handleBtnGrp('search')}>{lang.EMPLOYEE_VN.search}</Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item onClick={() => handleBtnGrp('export')}>{lang.EMPLOYEE_VN.export}</Dropdown.Item>
                      </SplitButton>
                    </InputGroup>
                  </div>
                  <BootstrapTable
                    {...props.baseProps}
                    rowEvents={{
                      onClick: (e, row, rowIndex) => {
                        console.log('click', { e, row, rowIndex });
                      }
                    }}
                    bootstrap4
                    striped
                    pagination={paginationFactory(pagination)}
                    noDataIndication={GLOBAL_TABLE_VN.tableEmpty}
                  />
                </>
              )}
            </ToolkitProvider>
          </SimpleCard>
        </Col>
      </Row>
    </>
  );
}
