import { createAction } from 'redux-actions';

export const handleSubmit = createAction('AUTHENTICATION/HANDLE_SUBMIT');
export const handleSubmitSuccessed = createAction('AUTHENTICATION/HANDLE_SUBMIT_SUCCESS');
export const handleSubmitFailed = createAction('AUTHENTICATION/HANDLE_SUBMIT_FAIL');
