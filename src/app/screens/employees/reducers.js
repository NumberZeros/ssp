/**
 * @file reducer
 */

// Using to control stage

import freeze from 'deep-freeze';
import { handleActions } from 'redux-actions';
import * as actions from './actions';

export const name = 'employees';

const initialState = freeze({
  data: {
    employeeNumber: 'NV 1',
    firstName: 'Nguyễn Thanh',
    lastName: 'Thọ',
    dob: new Date(),
    gender: 'male',
    phone: '0396171265',
    email: 'thotpbank050699@gmail.com',
    address: 'Quận 9, TP.HCM',
    village: 'Thiện Trí',
    district: 'Cái Bè',
    city: '',
    province: 'Tiền Giang',
    certificate: '123456789',
    notePerson: '',
    suppervisor: 'Thanh Thọ',
    department: 'Phát triển sản phẩm',
    position: 'Project Manager',
    salary: '10.000.000',
    typePayment: 'Ngân Hàng ACB',
    numberPayment: 123456789,
    noteDepartment: '',
    nameQuanlifi: 'Đại Học',
    level: 5,
    noteQuanlifi: 'Không có',
    createdAt: new Date()
  }
});

export default handleActions(
  {
    [actions.getInfoEmployee]: (state, action) => {
      return freeze({
        ...state,
        isSubmited: true
      });
    },
    [actions.getInfoEmployeeSuccessed]: (state, action) => {
      return freeze({
        ...state,
        isSubmited: false,
        data: action.payload,
        isAuthencated: true
      });
    }
  },
  initialState
);
