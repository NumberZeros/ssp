import { call, put } from 'redux-saga/effects';
import * as actions from '../actions';
import { takeAction } from '../../../services/forkActionSagas';

export function* handleSubmitCallBack(action) {
  try {
    console.log('handleSubmit', action.payload);
    const { data } = action.payload;
    yield put(actions.handleSubmitSuccessed(data));
  } catch (error) {
    yield put(actions.handleSubmitFailed(error));
  }
}
export function* handleSubmit() {
  yield takeAction(actions.handleSubmit, handleSubmitCallBack);
}
export default [handleSubmit];
