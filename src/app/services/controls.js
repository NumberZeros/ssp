import cookie from "js-cookie";

export const get = async (name: string) => {
  try {
    let data = await cookie.get(name);
    return data;
  } catch (err) {
    throw err;
  }
};

export const getHeaders = async () => {
  try {
    let data = await cookie.get("token");
    return data;
  } catch (err) {
    throw err;
  }
};

export const save = async (name: string, value: any) => {
  try {
    await cookie.set(name, value);
    return true;
  } catch (err) {
    throw err;
  }
};

export const clear = (name: string) => {
  cookie.remove(name);
};

export default {
  get,
  save,
  clear,
};
