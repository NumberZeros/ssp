import { lazy } from 'react';

import * as ROUTERS from '../../../constants/router';

const Container = lazy(() => import('./Container'));

const routers = [
  {
    path: ROUTERS.EMPLOYEES.DEFAULT,
    component: Container
  }
];

export default routers;
