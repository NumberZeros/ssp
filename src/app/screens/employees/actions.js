import { createAction } from 'redux-actions';

export const getInfoEmployee = createAction('EMPLOYEE/GET_INFOR');
export const getInfoEmployeeSuccessed = createAction('EMPLOYEE/GET_INFOR_SUCCESS');
export const getInfoEmployeeFailed = createAction('EMPLOYEE/GET_INFOR_FAIL');
