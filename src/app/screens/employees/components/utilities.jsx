import React from 'react';

import * as lang from './lang';
import moment from 'moment';
import { GLOBAL_TABLE_VN } from '../../../constants/table.lang';
import { NavLink } from 'react-router-dom';
import { Dropdown, DropdownButton } from 'react-bootstrap';

export const columns = [
  {
    dataField: 'index',
    text: 'No',
    sort: true,
    headerStyle: { width: '5em' }
  },
  {
    dataField: 'name',
    text: lang.EMPLOYEE_VN.name,
    headerAlign: 'center'
  },
  {
    dataField: 'email',
    text: lang.EMPLOYEE_VN.email,
    headerAlign: 'center'
  },
  {
    dataField: 'position',
    text: lang.EMPLOYEE_VN.position,
    headerAlign: 'center',
    align: 'center'
  },
  {
    dataField: 'address',
    text: lang.EMPLOYEE_VN.address,
    headerAlign: 'center'
  },
  {
    dataField: 'createdAt',
    text: lang.EMPLOYEE_VN.createdAt,
    align: 'center',
    headerAlign: 'center'
  },
  {
    dataField: 'action',
    text: '',
    align: 'center',
    headerAlign: 'center'
  }
];

export const paginationOptions = {
  ...GLOBAL_TABLE_VN,
  paginationSize: 15,
  pageStartIndex: 1,
  showTotal: true,
  totalSize: lang.MOCK_DATA.length
};

export const mapDataTable = (data) =>
  data.map((o, index) => ({
    ...o,
    index: index + 1,
    name: (
      <NavLink
        to={`?id=${index + 1}`}
        target="_blank"
        rel="noopener noreferrer">{`${o.lastName} ${o.firstName}`}</NavLink>
    ),
    createdAt: moment(o.createdAt).format('DD/MM/YYYY'),
    action: (
      <DropdownButton variant="outline-primary" title={<i className="i-Pen-4" />}>
        <Dropdown.Item className="m-0, p-0" eventKey="1">
          <NavLink to={`?id=${index + 1}`} className="d-flex p-2 h-100 w-100" rel="noopener noreferrer">
            {lang.EMPLOYEE_VN.view}
          </NavLink>
        </Dropdown.Item>
        <Dropdown.Item className="m-0, p-0" eventKey="2">
          <NavLink to={`?id=${index + 1}&isSaved=T`} className="d-flex p-2 h-100 w-100" rel="noopener noreferrer">
            {lang.EMPLOYEE_VN.update}
          </NavLink>
        </Dropdown.Item>
      </DropdownButton>
    )
  }));
