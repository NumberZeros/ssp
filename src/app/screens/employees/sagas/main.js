import { call, put } from 'redux-saga/effects';
import * as actions from '../actions';
import { takeAction } from '../../../services/forkActionSagas';

export function* getInfoEmployeeCallBack(action) {
  try {
    const { data } = action.payload;
    yield put(actions.getInfoEmployeeSuccessed(data));
  } catch (error) {
    yield put(actions.getInfoEmployeeFailed(error));
  }
}
export function* getInfoEmployee() {
  yield takeAction(actions.getInfoEmployee, getInfoEmployeeCallBack);
}
export default [getInfoEmployee];
