import React, { useState } from 'react';
import { Formik } from 'formik';
import * as yup from 'yup';
import { Form } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import DateTime from 'react-datetime';

import { useDispatch } from 'react-redux';
import { actions, name } from '..';
import { useSelector } from 'react-redux';

import * as lang from './lang';
import { GLOBAL_GENDER, GLOBAL_LOCATION } from '../../../constants/options';

function Signup() {
  const { data } = useSelector((state) => state[name]);

  const localState = useState(null);
  const history = useHistory();
  const handleSubmit = (values, { setSubmitting }) => {
    console.log('submit', values);
    history.push('/dashboard/v2');
  };
  return (
    <div
      className="auth-layout-wrap"
      style={{
        backgroundImage: 'url(/assets/images/photo-wide-4.jpg)'
      }}>
      <div className="auth-content" style={{ height: '80vh' }}>
        <div className="card o-hidden" style={{ height: '80vh', overflow: 'auto' }}>
          <h1 className="mb-3 mt-3 text-center text-24">{lang.SIGN_UP_VN.title}</h1>
          <div className="p-4" style={{ overflow: 'auto', height: '100%' }}>
            <Formik initialValues={data} validationSchema={SignupSchema} onSubmit={handleSubmit}>
              {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => {
                const idx = GLOBAL_LOCATION.findIndex((o) => o.province.toUpperCase() === values.province);
                const districtOption =
                  idx > -1
                    ? GLOBAL_LOCATION[idx].districts.map((o, index) => (
                        <option key={`${o} ${index}`} value={o.toUpperCase()}>
                          {o}
                        </option>
                      ))
                    : [];
                const idxCompany = GLOBAL_LOCATION.findIndex(
                  (o) => o.province.toUpperCase() === values.provinceCompany
                );
                const districtCompanyOption =
                  idxCompany > -1
                    ? GLOBAL_LOCATION[idxCompany].districts.map((o, index) => (
                        <option key={`${o} ${index}`} value={o.toUpperCase()}>
                          {o}
                        </option>
                      ))
                    : [];
                return (
                  <form onSubmit={handleSubmit}>
                    <div className="row">
                      <div className="col-md-12 card-header bg-transparent">
                        <h3 className="card-title">{lang.SIGN_UP_VN.primaryInfo}</h3>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.firstName} (*)</label>
                          <input
                            className="form-control form-control-rounded"
                            name="firstName"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          {errors.firstName && touched.firstName && (
                            <div className="text-danger mt-1 ml-2">{errors.firstName}</div>
                          )}
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.lastName} (*)</label>
                          <input
                            className="form-control form-control-rounded"
                            name="lastName"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          {errors.lastName && touched.lastName && (
                            <div className="text-danger mt-1 ml-2">{errors.lastName}</div>
                          )}
                        </div>
                      </div>
                      <div className="col-md-6 form-group mb-3">
                        <label htmlFor="picker1">{lang.SIGN_UP_VN.gender}</label>
                        <select
                          id="gender"
                          className="form-control form-control-rounded"
                          name="gender"
                          onChange={handleChange}
                          onBlur={handleBlur}>
                          {GLOBAL_GENDER.map((o) => (
                            <option value={o}>{lang.SIGN_UP_VN[o]}</option>
                          ))}
                        </select>
                        {errors.select && touched.select && (
                          <div className="text-danger mt-1 ml-2">{errors.select}</div>
                        )}
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.dob}</label>
                          <DateTime className="form-control-rounded" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.phone}</label>
                          <input
                            className="form-control form-control-rounded"
                            name="phone"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="email">{lang.SIGN_UP_VN.email}</label>
                          <input
                            className="form-control form-control-rounded"
                            name="email"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-md-4">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.address}</label>
                          <input
                            className="form-control form-control-rounded"
                            name="address"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="picker1">{lang.SIGN_UP_VN.province}</label>
                        <select
                          id="province"
                          className="form-control form-control-rounded"
                          name="province"
                          onser
                          onChange={handleChange}
                          onBlur={handleBlur}>
                          {GLOBAL_LOCATION.map((o) => (
                            <option key={o.province.toLocaleUpperCase()} value={o.province.toUpperCase()}>
                              {o.province}
                            </option>
                          ))}
                        </select>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="picker1">{lang.SIGN_UP_VN.district}</label>
                        <select
                          id="district"
                          className="form-control form-control-rounded"
                          name="district"
                          onChange={handleChange}
                          onBlur={handleBlur}>
                          {districtOption}
                        </select>
                      </div>
                      <div className="col-md-12 card-header bg-transparent">
                        <h3 className="card-title">{lang.SIGN_UP_VN.brandInfo}</h3>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.brandName} (*)</label>
                          <input
                            className="form-control form-control-rounded"
                            name="brandName"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>{' '}
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.type} (*)</label>
                          <input
                            className="form-control form-control-rounded"
                            name="type"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>{' '}
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.quantityEmployee}</label>
                          <input
                            className="form-control form-control-rounded"
                            name="quantityEmployee"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>{' '}
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.certificateBussiness}</label>
                          <input
                            className="form-control form-control-rounded"
                            name="certificateBussiness"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>{' '}
                      <div className="col-md-4">
                        <div className="form-group">
                          <label htmlFor="text">{lang.SIGN_UP_VN.address}</label>
                          <input
                            className="form-control form-control-rounded"
                            name="addressCompany"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="picker1">{lang.SIGN_UP_VN.province}</label>
                        <select
                          id="province"
                          className="form-control form-control-rounded"
                          name="provinceCompany"
                          onser
                          onChange={handleChange}
                          onBlur={handleBlur}>
                          {GLOBAL_LOCATION.map((o) => (
                            <option key={o.province.toLocaleUpperCase()} value={o.province.toUpperCase()}>
                              {o.province}
                            </option>
                          ))}
                        </select>
                      </div>
                      <div className="col-md-4">
                        <label htmlFor="picker1">{lang.SIGN_UP_VN.district}</label>
                        <select
                          id="districtCompany"
                          className="form-control form-control-rounded"
                          name="district"
                          onChange={handleChange}
                          onBlur={handleBlur}>
                          {districtCompanyOption}
                        </select>
                      </div>
                      <div className="col-md-12 card-header bg-transparent">
                        <h3 className="card-title">{lang.SIGN_UP_VN.authenticateInfo}</h3>
                      </div>
                      <div className="col-md-12">
                        <div className="form-group">
                          <label htmlFor="username">{lang.SIGN_UP_VN.username} (*)</label>
                          <input
                            className="form-control form-control-rounded"
                            name="username"
                            type="text"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          {errors.username && touched.username && (
                            <div className="text-danger mt-1 ml-2">{errors.username}</div>
                          )}
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="password">{lang.SIGN_UP_VN.password} (*)</label>
                          <input
                            name="password"
                            className="form-control form-control-rounded"
                            type="password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          {errors.password && touched.password && (
                            <div className="text-danger mt-1 ml-2">{errors.password}</div>
                          )}
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="repassword">{lang.SIGN_UP_VN.confirmPassword} (*)</label>
                          <input
                            name="confirmPassword"
                            className="form-control form-control-rounded"
                            type="password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          {errors.confirmPassword && touched.confirmPassword && (
                            <div className="text-danger mt-1 ml-2">{errors.confirmPassword}</div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="form-group">
                        <Form.Check
                          name="agrement"
                          onChange={handleChange}
                          value={values.switch}
                          type="checkbox"
                          id="custom-switch"
                          label={lang.SIGN_UP_VN.agrement}
                        />
                      </div>
                    </div>
                    <button className="btn btn-primary w-100 my-1 btn-rounded mt-3" type="submit">
                      {lang.SIGN_UP_VN.btnSignUp}
                    </button>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}

const SignupSchema = yup.object().shape({
  firstName: yup.string().required(`${lang.SIGN_UP_VN.firstName} ${lang.SIGN_UP_VN.cautionMandantory}`),
  lastName: yup.string().required(`${lang.SIGN_UP_VN.lastName} ${lang.SIGN_UP_VN.cautionMandantory}`),
  phone: yup.string().required(`${lang.SIGN_UP_VN.phone} ${lang.SIGN_UP_VN.cautionMandantory}`),
  brandName: yup.string().required(`${lang.SIGN_UP_VN.brandName} ${lang.SIGN_UP_VN.cautionMandantory}`),
  username: yup.string().required(`${lang.SIGN_UP_VN.username} ${lang.SIGN_UP_VN.cautionMandantory}`),
  email: yup.string().email(lang.SIGN_UP_VN.cautionEmailFomat),
  password: yup
    .string()
    .min(8, lang.SIGN_UP_VN.cautionPassowordLength)
    .required(`${lang.SIGN_UP_VN.password} ${lang.SIGN_UP_VN.cautionMandantory}`),
  confirmPassword: yup.string().required(`${lang.SIGN_UP_VN.confirmPassword} ${lang.SIGN_UP_VN.cautionMandantory}`),
  confirmPassword: yup
    .string()
    .required(`${lang.SIGN_UP_VN.confirmPassword} ${lang.SIGN_UP_VN.cautionMandantory}`)
    .oneOf([yup.ref('password')], lang.SIGN_UP_VN.cautionPassowordRepect)
});

export default Signup;
