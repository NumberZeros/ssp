import { all } from 'redux-saga/effects';
import { sagas as Authen } from '../screens/sessions';
import { sagas as Employees } from '../screens/employees';

export default function* rootSaga() {
  yield all([Authen(), Employees()]);
}
