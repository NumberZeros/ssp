import { combineReducers } from 'redux';
import LoginReducer from './reducers/LoginReducer';
import UserReducer from './reducers/UserReducer';
import LayoutReducer from './reducers/LayoutReducer';
import ScrumBoardReducer from './reducers/ScrumBoardReducer';
import NotificationReducer from './reducers/NotificationReducer';
import EcommerceReducer from './reducers/EcommerceReducer';

import Authen, { name as nameOfAuthen } from '../screens/sessions';
import Employees, { name as nameOfEmployees } from '../screens/employees';

export const staticReducers = {
  login: LoginReducer,
  user: UserReducer,
  layout: LayoutReducer,
  scrumboard: ScrumBoardReducer,
  notification: NotificationReducer,
  ecommerce: EcommerceReducer,
  [nameOfAuthen]: Authen,
  [nameOfEmployees]: Employees
};

export default combineReducers(staticReducers);
