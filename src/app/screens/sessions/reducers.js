/**
 * @file reducer
 */

// Using to control stage

import freeze from 'deep-freeze';
import { handleActions } from 'redux-actions';
import * as actions from './actions';

export const name = 'authentication';

const initialState = freeze({
  data: {
    firstName: '',
    lastName: '',
    dob: '',
    gender: '',
    phone: '',
    email: '',
    address: '',
    village: '',
    district: '',
    city: '',
    province: '',
    brandName: '',
    type: '',
    quantityEmployee: '',
    certificateBussiness: '',
    authenticateInfo: '',
    username: '',
    password: '',
    confirmPassword: '',
    remindPassword: '',
    agrement: ''
  },
  isSubmited: false,
  isAuthencated: false
});

export default handleActions(
  {
    [actions.handleSubmit]: (state, action) => {
      return freeze({
        ...state,
        isSubmited: true
      });
    },
    [actions.handleSubmitSuccessed]: (state, action) => {
      console.log('reducer', action);
      return freeze({
        ...state,
        isSubmited: false,
        data: action.payload,
        isAuthencated: true
      });
    }
  },
  initialState
);
