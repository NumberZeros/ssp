import { lazy } from 'react';

import * as ROUTERS from '../../../constants/router';

const Signup = lazy(() => import('./Signup'));

const Signin = lazy(() => import('./Signin'));

const ForgotPassword = lazy(() => import('./ForgotPassword'));

const Error404 = lazy(() => import('./Error'));

const sessionsRoutes = [
  {
    path: ROUTERS.SESSION.SIGN_UP,
    component: Signup
  },
  {
    path: ROUTERS.SESSION.SIGN_IN,
    component: Signin
  },
  {
    path: ROUTERS.SESSION.FORGOT_PASSWORD,
    component: ForgotPassword
  },
  {
    path: ROUTERS.SESSION.ERROR,
    component: Error404
  }
];

export default sessionsRoutes;
