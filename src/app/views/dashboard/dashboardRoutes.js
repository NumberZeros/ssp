import { lazy } from 'react';
import { authRoles } from 'app/auth/authRoles';
import * as ROUTERS from '../../constants/router';

const Dashboard1 = lazy(() => import('./dashboard1/Dashboard1'));

const Dashboard2 = lazy(() => import('./dashboard2/Dashboard2'));

const dashboardRoutes = [
  {
    path: ROUTERS.DASHBOARD.DEFAULT,
    component: Dashboard1,
    auth: authRoles.admin
  },
  {
    path: ROUTERS.DASHBOARD.DASHBOARD_V2,
    component: Dashboard2,
    auth: authRoles.admin
  }
];

export default dashboardRoutes;
