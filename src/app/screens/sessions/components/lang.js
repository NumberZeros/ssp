export const SIGN_UP_VN = {
  title: 'Đăng ký',
  //fields
  primaryInfo: 'Thông tin đại người diện cửa hàng',
  firstName: 'Họ',
  lastName: 'Tên',
  dob: 'Ngày sinh',
  gender: 'Giới Tính',
  phone: 'Điện Thoại',
  email: 'Địa chỉ E-mail',
  address: 'Địa chỉ',
  village: 'Xã',
  district: 'Huyện',
  city: 'Thành Phố',
  province: 'Tỉnh',
  brandInfo: 'Thông tin cửa hàng',
  brandName: 'Tên cửa hàng',
  type: 'Loại hình kinh doanh',
  quantityEmployee: 'Số lượng nhân viên',
  certificateBussiness: 'Mã giấy phép kinh doanh',
  authenticateInfo: 'Thông tin đăng nhập',
  username: 'Tên tài khoản',
  password: 'Mật khẩu',
  confirmPassword: 'Xác nhận mật khẩu',
  remindPassword: 'Mật khẩu gợi ý',
  agrement: 'Tôi đồng ý với các điều khoản và qui định khi sử dụng phần mềm',
  // button
  btnSignUp: 'Đăng ký',
  // alert
  alrtSuccess: 'Thành Công',
  alrtSuccessDescription: 'Chúc mừng bạn đã đến với ngôi nhà chung SSP',
  alrtFail: 'Thất bại',
  alrtFailDescription: 'Vui lòng kiểm tra lại',
  //caution
  cautionMandantory: 'không được trống',
  cautionEmailFomat: 'Sai định dạng E-mail',
  cautionPassowordLength: 'Mật khẩu phải ít nhất có 8 ký tự',
  cautionPassowordRepect: 'Mật khẩu sác nhận chưa đúng',
  // options
  male: 'Nam',
  female: 'Nữ',
  other: 'Khác'
};
