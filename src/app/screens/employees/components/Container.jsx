import React, { useEffect, useState } from 'react';

import { useHistory } from 'react-router-dom';
import { useQuery } from '../../../services/routerServices';

import TableEmployee from './Table';
import EmployeeDetail from './Employee_Detail';
import FormEmployee from './Form';
function Container() {
  const history = useHistory();
  const query = useQuery();
  const [dataQuery, setQuery] = useState(null);
  useEffect(() => {
    const id = query.get('id');
    const isSaved = query.get('isSaved');

    if (!!id)
      setQuery({
        id,
        isSaved
      });
    else setQuery(null);
  }, [history.location]);
  return (
    <>
      {!dataQuery && <TableEmployee />}
      {!!dataQuery && dataQuery.isSaved == 'T' && <FormEmployee />}
      {!!dataQuery && !!dataQuery.id && <EmployeeDetail />}{' '}
    </>
  );
}

export default Container;
