export const DASHBOARD = {
  DEFAULT: '/shop/dashboard',
  DASHBOARD_V2: '/shop/dashboard/v2'
};

export const EMPLOYEES = {
  DEFAULT: '/shop/employees'
};

export const SESSION = {
  ERROR: '/session/404',
  SIGN_IN: '/session/signin',
  SIGN_UP: '/session/signup',
  FORGOT_PASSWORD: '/session/forgot-password'
};
