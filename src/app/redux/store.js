import thunk from 'redux-thunk';

import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import RootReducer from './reducers.js';
import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger();
const middlewares = [sagaMiddleware, thunk];

if (process.env.NODE_ENV === `development`) {
  middlewares.push(loggerMiddleware);
}
export const Store = createStore(RootReducer, {}, compose(applyMiddleware(...middlewares)));
sagaMiddleware.run(sagas);
